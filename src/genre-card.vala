[GtkTemplate (ui = "/com/github/pamugk/Polyhymnia/ui/genre-card.ui")]
class Polyhymnia.GenreCard : Gtk.Box {
    [GtkChild]
    private unowned Gtk.Image image;
    [GtkChild]
    private unowned Gtk.Label name_label;

    public GenreCard (Domain.Genre genre) {
        Object(orientation: Gtk.Orientation.VERTICAL, spacing: 4);
        name_label.label = genre.name;
    }
}

