public class Polyhymnia.Application : Adw.Application {
    public Application () {
        Object (application_id: "com.github.pamugk.Polyhymnia", flags: ApplicationFlags.DEFAULT_FLAGS);
    }

    construct {
        ActionEntry[] action_entries = {
            { "about", this.on_about_action },
            { "preferences", this.on_preferences_action },
            { "quit", this.quit }
        };
        this.add_action_entries (action_entries, this);
        this.set_accels_for_action ("app.preferences", {"<primary>comma"});
        this.set_accels_for_action ("app.quit", {"<primary>q"});
    }

    public override void activate () {
        base.activate ();
        var win = this.active_window;
        if (win == null) {
            win = new Polyhymnia.Window (this);
        }
        win.present ();
    }

    private void on_about_action () {
        string[] developers = { "Admin" };
        var about = new Adw.AboutWindow () {
            transient_for = this.active_window,
            application_name = _("Polyhymnia Music Player"),
            application_icon = "com.github.pamugk.Polyhymnia",
            developer_name = "pamugk",
            version = Constants.Config.VERSION,
            developers = developers,
            copyright = "© 2023 pamugk",
        };

        about.present ();
    }

    private void on_preferences_action () {
        var preferences_window = new Polyhymnia.PreferencesWindow();
        preferences_window.present();
    }
}

