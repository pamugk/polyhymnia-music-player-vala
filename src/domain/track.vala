public struct Polyhymnia.Domain.Track {
    long id;

    Album album;
    Artist artist;
    string? composer;
    Genre? genre;
    int disk;
    TimeSpan duration;
    string file_path;
    int position;
    string? name;
}

