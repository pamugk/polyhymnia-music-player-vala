public struct Polyhymnia.Domain.Album {
    long id;

    Artist? artist;
    DateTime date;
    Genre? genre;
    string title;
}

