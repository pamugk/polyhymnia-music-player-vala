[GtkTemplate (ui = "/com/github/pamugk/Polyhymnia/ui/album-card.ui")]
class Polyhymnia.AlbumCard : Gtk.Box {
    [GtkChild]
    private unowned Gtk.Image cover_image;
    [GtkChild]
    private unowned Gtk.Label album_label;
    [GtkChild]
    private unowned Gtk.Label artist_label;
    [GtkChild]
    private unowned Gtk.Label date_label;

    public AlbumCard (Domain.Album album) {
        Object(orientation: Gtk.Orientation.VERTICAL, spacing: 4);
        album_label.label = album.title;
        artist_label.label = album.artist?.name;
        date_label.label = album.date.format("%x");
    }
}
