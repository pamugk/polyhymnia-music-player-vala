[GtkTemplate (ui = "/com/github/pamugk/Polyhymnia/ui/track-row.ui")]
class Polyhymnia.TrackRow : Gtk.Grid {
    [GtkChild]
    private unowned Gtk.Image album_cover_image;
    [GtkChild]
    private unowned Gtk.Label track_label;
    [GtkChild]
    private unowned Gtk.Label artist_label;
    [GtkChild]
    private unowned Gtk.Label album_label;
    [GtkChild]
    private unowned Gtk.Label duration_label;

    public TrackRow (Domain.Track track) {
        track_label.label = track.name == null
            ? Path.get_basename(track.file_path)
            : track.name;
        duration_label.label = Utils.format_timespan(track.duration);

        album_label.label = track.album.title == null
            ? _("Unknown album")
            : track.album.title;
        artist_label.label = track.artist.name == null
            ? _("Unknown artist")
            : track.artist.name;
    }
}
