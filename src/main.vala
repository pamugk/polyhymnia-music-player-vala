
int main (string[] args) {
    Intl.setlocale (LocaleCategory.ALL, "");
    Intl.bindtextdomain (Constants.Config.GETTEXT_PACKAGE, Constants.Config.LOCALEDIR);
    Intl.bind_textdomain_codeset (Constants.Config.GETTEXT_PACKAGE, "UTF-8");
    Intl.textdomain (Constants.Config.GETTEXT_PACKAGE);
    var app = new Polyhymnia.Application ();
    return app.run (args);
}
