namespace Polyhymnia.Utils {
    static string format_timespan(TimeSpan timespan) {
        TimeSpan full_seconds = timespan / TimeSpan.SECOND;
        int hours = (int) full_seconds / (60 * 60);
        int minutes = (int) (full_seconds / 60) % 60;
        int seconds = (int) full_seconds % 60;
        return hours == 0
            ? "%02d:%02d".printf(minutes, seconds)
            : "%d:%02d:%02d".printf(hours, minutes, seconds);
    }
}

