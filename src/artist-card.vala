[GtkTemplate (ui = "/com/github/pamugk/Polyhymnia/ui/artist-card.ui")]
class Polyhymnia.ArtistCard : Gtk.Box {
    [GtkChild]
    private unowned Adw.Avatar avatar;
    [GtkChild]
    private unowned Gtk.Label name_label;

    public ArtistCard (Domain.Artist artist) {
        Object(orientation: Gtk.Orientation.VERTICAL, spacing: 4);
        avatar.text = artist.name;
        name_label.label = artist.name;
    }
}

