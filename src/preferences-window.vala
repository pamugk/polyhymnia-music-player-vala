[GtkTemplate (ui = "/com/github/pamugk/Polyhymnia/ui/preferences-window.ui")]
private class Polyhymnia.PreferencesWindow : Adw.PreferencesWindow {
    [GtkChild]
    private unowned Gtk.Switch resume_playback_switch;

    [GtkChild]
    private unowned Gtk.Switch play_explicit_switch;
    [GtkChild]
    private unowned Gtk.Switch show_explicit_switch;
    [GtkChild]
    private unowned Gtk.Switch scan_startup_switch;
    [GtkChild]
    private unowned Gtk.Button scan_sources_add_button;
    [GtkChild]
    private unowned Adw.ExpanderRow scan_sources_expander_row;

    private Settings settings = new Settings("com.github.pamugk.Polyhymnia");
    private Array<string> scan_sources;

    construct {
        this.settings.bind("app-system-resume-playback", this.resume_playback_switch,
                           "active", SettingsBindFlags.DEFAULT);
        this.settings.bind("app-library-explicit-songs", this.play_explicit_switch,
                           "active", SettingsBindFlags.DEFAULT);
        this.settings.bind("app-library-explicit-covers", this.show_explicit_switch,
                           "active", SettingsBindFlags.DEFAULT);
        this.settings.bind("app-library-scan-startup", this.scan_startup_switch,
                           "active", SettingsBindFlags.DEFAULT);
        this.scan_sources = new Array<string>.take(this.settings.get_strv("app-library-scan-directories"), true);

        this.add_source("~/Music", false);
        foreach (var source in this.scan_sources) {
            this.add_source(source, true);
        }

        scan_sources_add_button.clicked.connect(this.open_source_dialog);
    }

    private void add_source(string source_path, bool user_defined) {
        var delete_button = new Gtk.Button.from_icon_name("user-trash-symbolic");
        delete_button.add_css_class("destructive-action");
        delete_button.valign = Gtk.Align.CENTER;
        delete_button.sensitive = user_defined;

        var source_row = new Adw.ActionRow();
        source_row.title = source_path;
        if (user_defined) {
            delete_button.clicked.connect((btn) => this.delete_source(source_row, source_path));
            source_row.subtitle = _("User-defined source");
        } else {
            source_row.subtitle = _("Default source");
        }
        source_row.add_suffix(delete_button);

        this.scan_sources_expander_row.add_row(source_row);
    }

    private void delete_source(Gtk.Widget row, string source_path) {
        var body_text = _("You are about to delete source with the following path: %s. Are you sure?").printf(source_path);
        var remove_source_dialog = new Adw.MessageDialog(this, _("Delete source?"), body_text);
        remove_source_dialog.add_response("cancel", _("_Cancel"));
        remove_source_dialog.add_response("delete", _("_Delete"));
        remove_source_dialog.set_close_response("cancel");
        remove_source_dialog.set_default_response("cancel");
        remove_source_dialog.set_response_appearance("delete", Adw.ResponseAppearance.DESTRUCTIVE);
        remove_source_dialog.response.connect((choice) => {
            if (choice == "delete") {
                bool found = false;
                uint deleted_index = 0;
                uint i = 0;
                foreach (var source in this.scan_sources) {
                    if (source == source_path) {
                        deleted_index = i;
                        found = true;
                        break;
                    }
                    i++;
                }
                if (found) {
                    this.scan_sources.remove_index(deleted_index);
                    this.scan_sources_expander_row.remove(row);
                    this.settings.set_strv("app-library-scan-directories", this.scan_sources.data);
                }
            }
        });
        remove_source_dialog.present();
    }

    private void open_source_dialog() {
        var add_source_dialog = new Gtk.FileDialog();
        add_source_dialog.select_folder.begin(this, null, (obj, result) => {
            File selected_source = ((Gtk.FileDialog) obj).select_folder.end(result);
            if (selected_source != null) {
                this.scan_sources.append_val(selected_source.get_path());
                add_source(selected_source.get_path(), true);
                this.settings.set_strv("app-library-scan-directories", this.scan_sources.data);
            }
        });
    }
}
