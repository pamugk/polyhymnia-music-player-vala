[GtkTemplate (ui = "/com/github/pamugk/Polyhymnia/ui/window.ui")]
class Polyhymnia.Window : Adw.ApplicationWindow {
    [GtkChild]
    private unowned Gtk.SearchBar searchbar;
    [GtkChild]
    private unowned Gtk.ToggleButton search;

    [GtkChild]
    private unowned Adw.ViewStack content_stack;

    [GtkChild]
    private unowned Gtk.ScrolledWindow artists_scrolled_window;
    [GtkChild]
    private unowned Gtk.ScrolledWindow albums_scrolled_window;
    [GtkChild]
    private unowned Gtk.ScrolledWindow tracks_scrolled_window;
    [GtkChild]
    private unowned Gtk.ScrolledWindow genres_scrolled_window;

    private Settings settings = new Settings ("com.github.pamugk.Polyhymnia");

    public Window (Gtk.Application app) {
        Object (application: app);
    }

    construct {
        this.search.toggled.connect(() => {
            searchbar.set_search_mode(search.active);
        });

        this.set_artists();
        this.set_albums();
        this.set_songs();
        this.set_genres();

        this.content_stack.visible_child_name = this.settings.get_string("app-system-start-page");

        this.settings.bind ("window-fullscreened", this, "fullscreened", SettingsBindFlags.DEFAULT);
        this.settings.bind ("window-height", this, "default-height", SettingsBindFlags.DEFAULT);
        this.settings.bind ("window-maximized", this, "maximized", SettingsBindFlags.DEFAULT);
        this.settings.bind ("window-width", this, "default-width", SettingsBindFlags.DEFAULT);
    }

    private void set_artists_not_found() {
        var status_page = new Adw.StatusPage();
        status_page.description = _("Try a library update");
        status_page.icon_name = "item-missing-symbolic";
        status_page.title = _("No artists found");
        this.artists_scrolled_window.child = status_page;
    }

    private void set_artists() {
        var container = new Gtk.FlowBox();
        container.column_spacing = 16;
        container.selection_mode = Gtk.SelectionMode.NONE;
        container.row_spacing = 12;
        for (int i = 1; i <= 5; i++) {
            container.append(new Polyhymnia.ArtistCard(Domain.Artist() {
                id = 0,
                name = @"Имя исполнителя $i"
            }));
        }
        this.artists_scrolled_window.child = container;
    }

    private void set_albums_not_found() {
        var status_page = new Adw.StatusPage();
        status_page.description = _("Try a library update");
        status_page.icon_name = "item-missing-symbolic";
        status_page.title = _("No albums found");
        this.albums_scrolled_window.child = status_page;
    }

    private void set_albums() {
        var container = new Gtk.FlowBox();
        container.column_spacing = 16;
        container.selection_mode = Gtk.SelectionMode.NONE;
        container.row_spacing = 12;
        for (int i = 1; i <= 5; i++) {
            container.append(new Polyhymnia.AlbumCard(Domain.Album(){
                id = 0,
                artist = Domain.Artist() {
                    id = 0,
                    name = "Имя"
                },
                genre = null,
                date = new DateTime.now_local(),
                title = @"Название альбома $i"
            }));
        }
        this.albums_scrolled_window.child = container;
    }

    private void set_songs_not_found() {
        var status_page = new Adw.StatusPage();
        status_page.description = _("Try a library update");
        status_page.icon_name = "item-missing-symbolic";
        status_page.title = _("No songs found");
        this.tracks_scrolled_window.child = status_page;
    }

    private void set_songs() {
        var container = new Gtk.ListBox();
        container.selection_mode = Gtk.SelectionMode.NONE;
        container.show_separators = true;
        for (int i = 1; i <= 5; i++) {
            container.append(new Polyhymnia.TrackRow(Domain.Track() {
                id = 0,

                album = Domain.Album() {
                    id = 0,
                    title = i < 3 ? null : "Название альбома"
                },
                artist = Domain.Artist() {
                    id = 0,
                    name = i == 1 ? null : "Имя исполнителя"
                },
                duration = TimeSpan.MINUTE + i * TimeSpan.SECOND,
                file_path = @"/home/user/Music/track$i.mp3",
                name = i % 2 == 0 ? @"Название трека $i" : null
            }));
        }
        this.tracks_scrolled_window.child = container;
    }

    private void set_genres_not_found() {
        var status_page = new Adw.StatusPage();
        status_page.description = _("Try a library update");
        status_page.icon_name = "item-missing-symbolic";
        status_page.title = _("No genres found");
        this.genres_scrolled_window.child = status_page;
    }

    private void set_genres() {
        var container = new Gtk.FlowBox();
        container.column_spacing = 16;
        container.selection_mode = Gtk.SelectionMode.NONE;
        container.row_spacing = 12;
        for (int i = 1; i <= 5; i++) {
            container.append(new Polyhymnia.GenreCard(Domain.Genre() {
                id = 0,
                name = @"Название жанра $i"
            }));
        }
        this.genres_scrolled_window.child = container;
    }
}
